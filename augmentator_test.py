import augmentator as a

import matplotlib.pyplot as plt

from PIL import Image

img0 = Image.open("./gui/image_g.png")
img1 = Image.open("./gui/image_d.png")
images = a.augmentation([img0, img1], horizontal=0.5)

plt.imshow(images[0])
plt.show()
plt.imshow(images[1])
plt.show()
