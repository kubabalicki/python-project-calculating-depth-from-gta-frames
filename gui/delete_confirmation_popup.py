from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
import ntpath


class DeleteLayout(BoxLayout):
    """Popup to confirm deleting photo"""

    def __init__(self, popup, image, image_panel):
        super().__init__()
        self.orientation = "vertical"
        self.image = image
        self.image_panel = image_panel
        self.popup = popup
        self._set_widgets()
        self._add_widgets()

    def _set_widgets(self):
        self.confirm_button = Button(text="Delete", size_hint_y=0.5)
        self.confirm_button.bind(on_press=self._delete_img)
        self.cancel_button = Button(text="Cancel", size_hint_y=0.5)
        self.cancel_button.bind(on_press=self._close)

    def _add_widgets(self):
        if self.image_panel.post_prediction:
            self.add_widget(
                Label(text="Delete this image and prediction?", size_hint_y=0.3)
            )
        else:
            self.add_widget(Label(text="Delete this image?", size_hint_y=0.3))
        self.add_widget(Image(source=self.image.source, size_hint_y=1.5))
        button_panel = BoxLayout(orientation="horizontal")
        button_panel.add_widget(self.cancel_button)
        button_panel.add_widget(self.confirm_button)
        self.add_widget(button_panel)

    def _delete_img(self, _):
        self.image.confirm_deleting()
        self._close(_)

    def _close(self, _):
        self.popup.dismiss()


class ClearLayout(BoxLayout):
    """Popup to confirm clearing"""

    def __init__(self, popup, image_panel):
        super().__init__()
        self.orientation = "vertical"
        self.image_panel = image_panel
        self.popup = popup
        self._set_widgets()
        self._add_widgets()

    def _set_widgets(self):
        self.confirm_button = Button(text="Clear")
        self.confirm_button.bind(on_press=self._clear)
        self.cancel_button = Button(text="Cancel")
        self.cancel_button.bind(on_press=self._close)

    def _add_widgets(self):
        self.add_widget(Label(text="Clear all images?"))
        button_panel = BoxLayout(orientation="horizontal")
        button_panel.add_widget(self.cancel_button)
        button_panel.add_widget(self.confirm_button)
        self.add_widget(button_panel)

    def _clear(self, _):
        self.image_panel.clear_panel()
        self._close(_)

    def _close(self, _):
        self.popup.dismiss()


class DeleteConfirmationPopup(Popup):
    """Popup to confirm deleting"""

    def __init__(self, image_panel, image=None, clear_all=False):
        super().__init__()
        if clear_all:
            self.title = "Clear"
            self.content = ClearLayout(self, image_panel)
            self.size = (500, 150)
        else:
            _, name = ntpath.split(image.source)
            self.title = "Deleting image: " + name
            self.content = DeleteLayout(self, image, image_panel)
            self.size = (500, 300)
        self.size_hint = (None, None)
        self.open()
