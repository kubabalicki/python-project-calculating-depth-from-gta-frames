from kivy.app import App
from kivy.core.window import Window
from panel import Panel
from kivy.config import Config

# turn off red dots on right click
Config.set("input", "mouse", "mouse, multitouch_on_demand")


class GUI(App):
    title = "Depth estimation Unet Model GUI"

    def build(self):
        Window.clearcolor = (0.5, 0.5, 0.5, 1)
        return Panel()


def main():
    GUI().run()


if __name__ == "__main__":
    main()
