from kivy.uix.textinput import TextInput


class TextPanel(TextInput):
    """TEXT LINE PANEL"""

    def __init__(self):
        super(TextPanel, self).__init__()
        self.size_hint = 1, 0.3
        self.background_color = 0.1, 0.1, 0.1, 1
        self.foreground_color = 1, 1, 1, 1
        self.readonly = True
