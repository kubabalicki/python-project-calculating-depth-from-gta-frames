from kivy.core.window import Window
from kivy.uix.popup import Popup
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.uix.image import Image
from kivy.core.image import Image as CoreImage
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.graphics.texture import Texture


class SavePopup(Popup):
    """Popup"""

    def __init__(self, text_panel, image_panel):
        super().__init__()
        self.title = "Delete unnecesary photos"
        self.content = MainPanel(self, text_panel, image_panel)
        self.size_hint = (None, None)
        self.size = (300, 500)
        self.open()


class OptionButton(Button):
    """Save or Cancel Button"""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.size_hint_y = None
        self.height = 50


class CustomSaveImage(Image, ButtonBehavior):
    """ IMAGE AFTER PREDICTION WITH SAVE ON-CLICK OPTION"""

    def __init__(self, texture, panel):
        super().__init__()
        self.texture = texture
        self.panel = panel

    def on_touch_down(self, touch):
        #
        # Deleting img
        #
        if self.collide_point(*touch.pos):
            self.panel.img.pop(self.panel.img.index(self))
            self.panel.refresh()

    def save(self, path):
        CoreImage(self.texture).save(path, flipped=True)


class ImageRow(BoxLayout):
    """ ROW OF IMAGES IN SCROLL PANEL """

    def __init__(self):
        super().__init__()
        self.orientation = "horizontal"
        self.size_hint = (None, None)
        self.height = 200
        self.width = 200


class PictureScroll(ScrollView):
    """ScrollView for showing preds to save"""

    def __init__(self, post_prediction):
        super().__init__()
        self.img = [CustomSaveImage(img.texture, self) for img in post_prediction]
        self.do_scroll_y = True
        self.size_hint = (None, None)
        self.width = 250
        self.height = 350
        self.rows = []
        self.__fill_image_panel()

    def __fill_vertical_panel(self):
        for _ in range(len(self.img)):
            temp = ImageRow()
            self.rows.append(temp)
            self.vertical_panel.add_widget(temp)

    def __fill_image_panel(self):
        self.vertical_panel = GridLayout(
            cols=1, spacing=(0, 10), padding=(25, 0), size_hint=(None, None)
        )
        self.vertical_panel.bind(minimum_height=self.vertical_panel.setter("height"))
        self.__fill_vertical_panel()
        for i in range(len(self.img)):
            self.rows[i].add_widget(self.img[i])
        self.add_widget(self.vertical_panel)

    def refresh(self):
        self.clear_widgets(self.children)
        self.rows = []
        #
        # Clear old parent
        #
        for x in self.img:
            x.parent = None
        self.__fill_image_panel()


class MainPanel(GridLayout):
    """Main Popup Panel"""

    def __init__(self, popup_parent, text_panel, image_panel):
        super().__init__()
        self.text_panel = text_panel
        self.image_panel = image_panel
        self.popup_parent = popup_parent

        self.cols = 1
        self.rows = 2
        self.padding = 10

        self.background_color = 0.1, 0.1, 0.1, 1
        self.foreground_color = 1, 1, 1, 1

        self.set_components()
        self.add_components()

    def set_components(self):
        self.panel = GridLayout(
            cols=2, padding=10, size_hint_y=None, height=100, spacing=(20, 0)
        )
        save_button = OptionButton(text="SAVE")
        cancel_button = OptionButton(text="CANCEL")
        save_button.bind(on_press=self.save_preds)
        cancel_button.bind(on_press=self.cancel)

        self.panel.add_widget(save_button)
        self.panel.add_widget(cancel_button)

        self.bg_panel = PictureScroll(self.image_panel.get_predictions())

    def add_components(self):
        self.add_widget(self.bg_panel)
        self.add_widget(self.panel)

    def cancel(self, _):
        self.popup_parent.dismiss()

    def save_preds(self, _):
        SaveInfoPopup(self.popup_parent, self.bg_panel.img, self.text_panel)


class SaveInfoPopup(Popup):
    """Popup Save Info"""

    def __init__(self, popup_parent, img, text_panel):
        super().__init__()
        self.title = (
            "Insert path and prefix\n" 'files will be saved under "path/prefix_#.jpg"'
        )
        self.popup_parent = popup_parent
        self.content = SavePanel(self, img, text_panel)
        self.size_hint = (None, None)
        self.size = (500, 200)
        self.open()


class SavePanel(GridLayout):
    """Save Popup Panel"""

    def __init__(self, popup_parent, img, text_panel):
        super().__init__()
        self.popup_parent = popup_parent
        self.img = img
        self.text_panel = text_panel

        self.cols = 1
        self.rows = 2
        self.padding = 10

        self.background_color = 0.1, 0.1, 0.1, 1
        self.foreground_color = 1, 1, 1, 1

        self.set_components()
        self.add_components()

    def set_components(self):
        self.panel_1 = GridLayout(
            cols=1, padding=5, size_hint_y=None, height=80, spacing=(0, 10)
        )
        self.panel = GridLayout(
            cols=2, padding=5, size_hint_y=None, height=50, spacing=(20, 0)
        )
        save_button = OptionButton(text="SAVE")
        save_button.height = 20
        cancel_button = OptionButton(text="CANCEL")
        cancel_button.height = 20
        save_button.bind(on_press=self.save_preds)
        cancel_button.bind(on_press=self.cancel)

        self.path = TextInput(text="PATH", multiline=False)
        self.path.size_hint_y = None
        self.path.height = 30

        self.prefix = TextInput(text="PREFIX", multiline=False)
        self.prefix.size_hint_y = None
        self.prefix.height = 30

        self.panel_1.add_widget(self.path)
        self.panel_1.add_widget(self.prefix)
        self.panel.add_widget(save_button)
        self.panel.add_widget(cancel_button)

    def add_components(self):
        self.add_widget(self.panel_1)
        self.add_widget(self.panel)

    def cancel(self, _):
        self.popup_parent.dismiss()

    def save_preds(self, _):
        #
        # Just save!
        #
        path = self.path.text
        if path[-1] != "/":
            path += "/"
        prefix = self.prefix.text
        for i in range(len(self.img)):
            try:
                self.img[i].save(path + prefix + "_" + str(i) + ".png")
            except Exception as ex:
                popup = Popup(
                    title="ERROR, click anywhere to exit",
                    content=Label(text=str(ex)),
                    size_hint=(None, None),
                    size=(500, 200),
                )
                popup.open()
                break
        else:
            self.text_panel.text += "Images saved under:\n" + path + "\n"
            self.popup_parent.popup_parent.dismiss()
            self.popup_parent.dismiss()
