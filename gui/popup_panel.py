from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.properties import StringProperty
from kivy.uix.image import Image
from kivy.uix.behaviors import ButtonBehavior


class OptionButton(Button):
    """ Panel with options: CONFIRM and CANCEL """

    def __init__(self, **kwargs):
        super(OptionButton, self).__init__(**kwargs)
        self.size_hint_y = None
        self.height = 50


class PhotosPanel(GridLayout):
    """ Panel with dragged pictures"""

    def __init__(self, **kwargs):
        super(PhotosPanel, self).__init__(**kwargs)
        self.cols = 3
        self.size_hint_y = None
        self.height = 300

    def add_image(self, file_path):
        self.add_widget(MyImage(file_path))

    def refresh(self):
        self.clear_widgets(self.children)
        self.parent.redraw()


class MyImage(Image, ButtonBehavior):
    """ Custom image with event"""

    def __init__(self, source):
        super(MyImage, self).__init__()
        self.source = source

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            self.parent.parent.file_path.remove(self.source)
            self.parent.refresh()


class PicturePanel(GridLayout):
    """ Main panel with popup components """

    def __init__(self, popup, text_panel, predict_btn, image_panel):
        super(PicturePanel, self).__init__()

        # dragged photos paths
        self.file_path = []
        self.popup = popup
        self.text_panel = text_panel
        self.predict_btn = predict_btn
        self.image_panel = image_panel

        self.cols = 1
        self.padding = 10

        self.background_color = 0.1, 0.1, 0.1, 1
        self.foreground_color = 1, 1, 1, 1

        self.set_components()
        self.add_components()

    def set_components(self):
        self.panel = GridLayout(cols=3, padding=10)
        self.confirm_button = OptionButton(text="CONFIRM")
        self.cancel_button = OptionButton(text="CANCEL")
        self.confirm_button.bind(on_press=self.save_photo)
        self.cancel_button.bind(on_press=self.cancel)

        self.panel.add_widget(self.cancel_button)
        self.panel.add_widget(self.confirm_button)

        self.bg_panel = PhotosPanel()
        self.info = "No picture/s choosen\n"

    def add_components(self):
        self.add_widget(self.bg_panel)
        self.add_widget(self.panel)

    def set_backround(self, file_path):
        """ Show photo in PhotosPanel """

        self.file_path.append(file_path)
        self.bg_panel.add_image(file_path)

    def redraw(self):
        """ Add widgets with pictures after deleting """
        for path in self.file_path:
            self.bg_panel.add_image(path)

    def cancel(self, _):
        """ Action for CANCEL button """
        self.info = "No picture/s choosen\n"
        self.popup.dismiss()

    def save_photo(self, _):
        """ Action for CONFIRM button """
        if self.file_path:
            self.info = "Loaded " + str(len(self.file_path)) + " pictures:\n"
            self.predict_btn.set_pred_image_path(self.file_path)
        self.popup.dismiss()
        self.image_panel.refresh(self.predict_btn.pred_images_paths)

    def send_info(self, _):
        """ Send info to text panel (console) """
        self.text_panel.text += self.info
        for p in self.file_path:
            self.text_panel.text += str(p) + "\n"
